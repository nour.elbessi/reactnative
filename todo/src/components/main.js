import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, TouchableOpacity, KeyboardAvoidingView, Platform } from 'react-native';
import Task from './Tasks/Task';
import { mainStyles } from './mainStyles';
import { Keyboard } from 'react-native';
import User from '../../data/User';
import api from '../../api';

const MainComponent = () => {
  const [task, setTask] = useState(null);
  const [taskItems, setTaskItems] = useState([]);
  const [user, setUser] = useState(new User('', '', ''));

  const onAddTask = () => {
    console.log(task);
    Keyboard.dismiss();
    setTaskItems([...taskItems, task]);
    setTask(null);
  };

  const onFinishedTask = (index) => {
    let itemsCopy = [...taskItems];
    itemsCopy.splice(index, 1);
    setTaskItems(itemsCopy);
  }
  useEffect(() => {
    const fetchUser = async () => {
      try {
        const response = await api.get('/sync/v9/user/');
        const userData = response.data;
        const parsedUser = new User(userData);
        setUser(parsedUser);
      } catch (error) {
        console.error('Error fetching user:', error);
      }
    };

    fetchUser();
  }, [user]);

  return (
    <View style={mainStyles.container}>
          <View>
      {/* <Text>User Details</Text>
      <Text>Email: {user.email}</Text>
      <Text>Name: {user.name}</Text>
      <Text>Avatar: {user.avatar}</Text> */}
    </View>
      <View style={mainStyles.tasksWrapper}>
        <Text style={mainStyles.sectionTitle}>Today's Tasks</Text>
        <View>
          {taskItems.map((item, index) => (
            //   <Task text={item} />

            <TouchableOpacity key={index} onPress={() => onFinishedTask(index)}>
              <Task text={item} />
            </TouchableOpacity>
          ))}
        </View>
      </View>
      <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'} style={mainStyles.writeTaskWrapper}>
        <TextInput
          style={mainStyles.input}
          placeholder={'Write a task'}
          value={task}
          onChangeText={(text) => setTask(text)}
        />
        <TouchableOpacity onPress={onAddTask}>
          <View style={mainStyles.addWrapper}>
            <Text style={mainStyles.addText}>+</Text>
          </View>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    </View>
  );
};

export default MainComponent;

