// import React from "react";
// import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
// import { taskStyles } from './taskStyles';

// const Task = (props) => {
//     return (
//         <View style={taskStyles.item}>
//         <View style={taskStyles.itemLeft}>
//             <TouchableOpacity style={taskStyles.square}></TouchableOpacity>
//             <Text style={taskStyles.itemText}>{props.text}</Text>
//         </View>
//         <View style={taskStyles.circular}></View>
//         </View>
//     )
// };
// export default Task;

import React, {useState} from "react";
import { View, Text, TouchableOpacity } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome'; 
import { taskStyles } from './taskStyles';

const Task = (props) => {

  const [isChecked, setChecked] = useState(false);

  const handleCheckboxToggle = () => {
    setChecked(!isChecked);
  };

  return (
    <View style={taskStyles.item}>
      <TouchableOpacity style={taskStyles.itemLeft} onPress={handleCheckboxToggle}>
      <Icon name={isChecked ? "check-square" : "square-o"} size={20}  style={taskStyles.checkbox} />
        <Text style={taskStyles.itemText}>{props.text}</Text>
      </TouchableOpacity>
  
    </View>
  );
};

export default Task;
