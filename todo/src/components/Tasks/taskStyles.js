// import { StyleSheet } from 'react-native';

// export const taskStyles = StyleSheet.create({
//   item: {
//     backgroundColor: 'white',
//     padding: 15,
//     borderRadius: 10,
//     marginBottom: 10,
//     flexDirection: 'row',
//     alignItems: 'center',
//     justifyContent: 'space-between',
//     marginBottom: 20,
//   },
//   itemText: {
//     fontSize: 16,
//   },
//     itemLeft: {
//         flexDirection: 'row',
//         alignItems: 'center',
//         flexWrap: 'wrap',
//     },
//     square: {
//         width: 24,
//         height: 24,
//         backgroundColor: 'blue',
//         opacity: 0.4,
//         borderRadius: 5,
//         marginRight: 15,
//     },
//     circular: {
//         width: 12,
//         height: 12,
//         borderColor: 'blue',
//         borderWidth: 2,
//         borderRadius: 5,
//     },

// });


import { StyleSheet } from 'react-native';

export const taskStyles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    padding: 15,
    borderRadius: 10,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  itemText: {
    fontSize: 16,
  },
  itemLeft: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  checkbox: {
    marginRight: 15,
  },
  dragIcon: {
    marginLeft: 15,
  },
});
