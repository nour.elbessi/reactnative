import axios from 'axios';

const TOKEN = 'b32a0b08f62627a23196f8f61a87ebdd44f62257';

const api = axios.create({
  baseURL: 'https://api.todoist.com/',
  headers: {
    Authorization: `Bearer ${TOKEN}`,
    'Content-Type': 'application/json',
  },
});

export default api;
