class User {
    constructor(email, name, avatar) {
      this.email = email || '';
      this.name = name || '';
      this.avatar = avatar || null;
    }
  }
  
  export default User;
  